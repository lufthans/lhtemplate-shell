# lhtemplate-shell

Template for creating a bash shell script.

It includes command line argument parsing, copyright declaration and base scaffolding for functions like help and usage.